# Rest API

- Esta é a API que você irá utilizar nos desafios.
- A API faz a listagem de lançamentos de um cartão de crédito dentro do periodo de N meses de um mesmo ano.
- A API está disponível em: https://my-json-server.typicode.com/marcusvseixas/simple-timeline.

**ATENÇÃO: Não será necessário usar nenhum outro método ou rota da API fora os descritos abaixo.**
 
### GET /lancamentos (https://my-json-server.typicode.com/marcusvseixas/simple-timeline/lancamentos)
Lista os lancamentos de cartão presente no sistema

**Response 200**
```json
[
  {
    "id": 1,
    "valor": 13.3,
    "origem": "Uber",
    "categoria": 1,
    "mes_lancamento": 1
  },
  {
    "id": 2,
    "valor": 130.5,
    "origem": "PS Store",
    "categoria": 2,
    "mes_lancamento": 2
  }
]
```
### GET /categorias (https://my-json-server.typicode.com/marcusvseixas/simple-timeline/categorias)
Lista as categorias de lançamento presentes no sistema

**Response 200**
```json
[
 {
    "id": 1,
    "nome": "Transporte"
  },
  {
    "id": 2,
    "nome": "Compras Online"
  }
]
```


> *Baseado em https://gitlab.com/desafio3/readme-api* 